deploy instruction

- run `composer install`
- run `composer require predis/predis`
- run `npm install`
- copy `.env.example to .env`
- run `php artisan key:generate`
- run `npm i --save laravel-echo socket.io-client`
- run `npm i -g laravel-echo-server pm2`
- run `laravel-echo-server init`
- run `php artisan serve`

Run The Server
    - run `laravel-echo-server start`

Stop The Server
    - run `laravel-echo-server stop`
