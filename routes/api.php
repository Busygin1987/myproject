<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//
// Route::get('profile', function () {
//     // Only verified users may enter...
// })->middleware('verified');
Route::pattern('id', '[0-9]+');

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::middleware('auth:api')
        ->group(function() {

  Route::get('logout', 'AuthController@logout');

  Route::prefix('message')
        ->group(function() {
          Route::get('/{id}', 'MessageController@getMessagesUser');
          Route::post('/', 'MessageController@sendMessage');
          Route::delete('/{id}', 'MessageController@deleteMessage');
  });

  Route::prefix('user')
        ->group(function() {
          Route::get('/', 'UserController@getUsers');
          Route::get('/{username}', 'UserController@getOneUser');
          Route::patch('/{id}', 'UserController@updateUser');
          Route::delete('/{id}', 'UserController@deleteUser');
  });
});
