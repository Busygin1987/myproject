<?php
namespace App\Repositories\Message;

interface MessageRepository
{
    /**
     * Get all messages User
     * @param int $id
     * @return mixed
     */
    public function getAllMessagesUser(int $id);

    /**
     * Store message
     * @param array $data
     * @return mixed
     */
    public function storeMessage(array $data);

    // /**
    //  * Send message
    //  * @param array $data
    //  * @return mixed
    //  */
    // public function sendMessage(array $data);

    /**
     * Delete message
     * @param int $id
     * @return mixed
     */
    public function deleteMessage(int $id);


}
