<?php

namespace App\Repositories\Message;

use App\Model\Message;

class MessageEloquent implements MessageRepository
{
    private $message;

    /**
     * MessageEloquent constructor.
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @param int $id User
     * @return array of messages
     */
    public function getAllMessagesUser(int $id)
    {
      $messages = $this->message->where(function($query) use ($id) {
          $query->where('user_id_from', Auth::id())
                ->where('user_id_to', $id);
          })
          ->orWhere(function($query) use ($id) {
              $query->where('user_id_from', $id)
                    ->where('user_id_to', Auth::id());
          })
          ->orderBy('created_at', 'desc')
          ->paginate(15);

      return $messages;
    }

    /**
     * @param array $data message
     * @return Message object
     * @throws \Exception
     */
    public function storeMessage(array $data)
    {
      $message = $this->message->create($data);

      if (empty($message)) {
        throw new \Exception('Failed to create message');
      }

      return $this->get($message['id']);
    }

    // /**
    //  * @param array $data message
    //  * @return Message object
    //  * @throws \Exception
    //  */
    // public function sendMessage(array $data)
    // {
    //   $message = $this->message->create($data);
    //
    //   if (empty($message)) {
    //     throw new \Exception('Failed to create message');
    //   }
    //
    //   return $this->get($message['id']);
    // }

    /**
     * @param int $id
     * @return boolean is trashed
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $item = $this->message->find($id);
        if (empty($item)) {
            throw new \Exception('Message not found');
        }

        return $item->delete();
    }
  }
