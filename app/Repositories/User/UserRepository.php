<?php
namespace App\Repositories\User;

interface UserRepository
{
    /**
     * Get all Users
     *
     * @return mixed
     */
    public function getAllUsers();

    /**
     * Get one User
     * @param string $username
     * @return mixed
     */
    public function getOneUser(string $username);

    /**
     * Update User
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function updateUser(array $data, int $id);

    /**
     * Delete User
     * @param int $id
     * @return mixed
     */
    public function deleteUser(int $id);


}
