<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\Message;

class UserEloquent implements UserRepository
{
    private $user;

    /**
     * UserEloquent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     *
     * @return array of users
     * @throws \Exception
     */
    public function getAllUsers()
    {
      $users = $this->user
                ->select(
                  'id',
                  'username',
                  'email',
                  'first_name',
                  'last_name')
                ->where('id', '!=', $this->user->id)
                ->get();

      if (empty($users)) {
        throw new \Exception('Users not found!');
      }
      return $users;
    }

    /**
     *
     * @param string $username
     * @return User object
     * @throws \Exception
     */
    public function getOneUser(string $username)
    {
      $user = $this->user
        ->where('username', $username)
        ->with(['albums', 'posts', 'friends', 'profile'])
        ->first();
      $messages = Message::where('user_id_to', $user->id)
                  ->orWhere('user_id_from', $user->id)
                  ->get();

      if (empty($user)) {
        throw new \Exception('User not found!');
      }
      return [
        'user' => $user,
        'messages' => $messages,
      ];
    }

     /**
     * @param int $id
     * @param array $data
     * @return User object
     * @throws \Exception
     */
    public function updateUser(array $data, int $id)
    {
      $user = $this->user->find($id);
      if (empty($user)) {
        throw new \Exception('User not found');
      }
      $user->update($data);

      return $this->get($id);
    }

    /**
     *
     * @param int $id
     * @return User object
     * @throws \Exception
     */
    public function deleteUser(int $id)
    {
      $user = $this->user->find($id);
      if (empty($user)) {
        throw new \Exception('User not found');
      }

      return $user->delete();
    }


  }
