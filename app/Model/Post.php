<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'user_id',
        'parent_id',
        'body',
        'attach',
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'like');
    }
    public function getMessage()
    {
        return $this->body;
    }
    public function comments()
    {
        return $this->hasMany('App\Models\Post', 'parent_id');
    }
}
