<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';
    protected $fillable = [
        'user_id',
        'path',
        'tag',
        'album_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like', 'user_id');
    }
}
