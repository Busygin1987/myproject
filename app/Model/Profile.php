<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
     protected $table = 'profiles';

     protected $fillable = [
       'birthday',
       'about',
       'photo_id',
       'status',
       'user_id',
       'location',
     ];

     public function user()
     {
         return $this->belongsTo('App\Models\User');
     }
     public function getLocation()
     {
         $location = Location::where('id', $this->location)
             ->firstOrFail();
         $location = [
           'country' => $location->country,
           'city' => $location->city,
         ];
         return $location;
     }
}
