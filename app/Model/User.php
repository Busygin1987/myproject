<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens;

    protected $table = 'users';

    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone_user',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne('App\Model\Profile');
    }

    public function friends()
    {
        return $this->hasMany('App\Model\Friend');
    }

    public function posts()
    {
        return $this->hasMany('App\Model\Post', 'user_id');
    }

    public function albums()
    {
        return $this->hasMany('App\Model\Album', 'user_id');
    }
}
