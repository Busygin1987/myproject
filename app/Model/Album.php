<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'albums';

    protected $fillable = [
      'name',
      'private',
      'user_id',
    ];

    public function user()
    {
       return $this->belongsTo('App\Models\User');
    }

    public function photos()
    {
       return $this->hasMany('App\Models\Photo');
    }
}
