<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dialog extends Model
{
  protected $table = 'dialogs';

  protected $fillable = [
     'from',
     'to',
  ];
}
