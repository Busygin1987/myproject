<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $fillable = [
        'user_id_from',
        'user_id_to',
        'text',
        'created_at',
        'read',
    ];
}
