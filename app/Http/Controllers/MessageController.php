<?php

namespace App\Http\Controllers;

use App\Model\Message;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Message\MessageRepository;
use App\Http\Requests\Message\MessageSendRequest;

class MessageController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $messageRepo;

    /**
     * MessageController constructor.
     * @param MessageRepository $messageRepo
     */
    public function __construct(MessageRepository $messageRepo)
    {
        $this->messageRepo = $messageRepo;
    }

    /**
    * Get all messages of user
    * @param int $id User
    * @return \Illuminate\Http\JsonResponse
    */
    public function getMessagesUser(int $id)
    {
        try {
            $messages = $this->messageRepo->getAllMessagesUser($id);

            return response()->json($messages, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
    * Send message
    * @param MessageSendRequest $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function sendMessage(MessageSendRequest $request)
    {
        try {
            $dataMessage = $request->only(['user_id_to', 'text']);

            $message = $this->messageRepo->storeMessage($dataMessage);

            event(new ChatMessage($message));

            return response()->json($message, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
    * Delete message
    * @param int $id User
    * @return \Illuminate\Http\JsonResponse
    */
    public function deleteMessage(int $id)
    {
        try {
            $result = $this->messageRepo->delete($id);

            return response()->json($result);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }
}
