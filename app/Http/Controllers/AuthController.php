<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Model\User;

class AuthController extends Controller
{
    /**
    * Login for users
    * @param LoginRequest $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            $response = [
                'user' => $user,
                'access_token' => $user->createToken('User token')->accessToken,
            ];
            return response()->json($response, 200);
        } else {
            return response()->json(['error' => 'Failed to login'], 401);
        }
    }

    /**
    * Register for guest
    * @param RegisterRequest $request
    * @return \Illuminate\Http\JsonResponse
    */
      public function register(RegisterRequest $request)
    {
        $data = $request->only([
          'username',
          'first_name',
          'last_name',
          'email',
          'password',
          'c_password',
        ]);

        $data['password'] = Hash::make($data['password']);

        try {
            $user = User::create($data);
            $response = [
                'user' => $user->username,
                'access_token' => $user->createToken('User token')->accessToken,
            ];

            return response()->json(['success' => $response], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

    }

    /**
    * Logout for users
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function logout()
    {
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            return response()->json(true, 200);
        } else {
            return response()->json(['error' => 'Failed to logout'], 400);
        }
    }

}
