<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\User\UserRepository;
use App\Http\Requests\User\UserUpdateRequest;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * UserController constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }
    /**
     * Get a listing of the users.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        try {
            $result = $this->userRepo->getAllUsers();

            return response()->json($result);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Get specific user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneUser(string $username)
    {
        try {
            $result = $this->userRepo->getOneUser($username);

            return response()->json($result);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Update specific user.
     * @param UserUpdateRequest $data
     * @param int $id User id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(UserUpdateRequest $data, int $id)
    {
        try {
            $result = $this->userRepo->updateUser($data, $id);

            return response()->json($result);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Delete user.
     * @param int $id User
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(int $id)
    {
        try {
            $result = $this->userRepo->deleteUser($id);

            return response()->json($result);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }
}
