<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Message\MessageRepository;
use App\Repositories\Message\MessageEloquent;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserEloquent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Repositories
        $this->app->singleton(MessageRepository::class, MessageEloquent::class);
        $this->app->singleton(UserRepository::class, UserEloquent::class);

    }
}
